package com.javagda11.reservation.repository;

import com.javagda11.reservation.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

//    @Query(nativeQuery = "select * from 1?")
//    public List<AppUser> select(String param);

    Optional<AppUser> findByUsername(String username);
}
